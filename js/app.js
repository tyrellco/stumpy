angular
  .module("stumpy", ["acRoot", "acHeader", "acStats", "acSidebar", "acFooter"])
  .service("resultService", function($http, $q) {
    return {
      getResults: getResults
    };
    function getResults(week) {
      var defered = $q.defer();
      var promise = defered.promise;
      var url = "http://localhost:8081/api/public/stats/scores";
      if (week > 0) {
        url = "http://localhost:8081/api/public/stats/scores/" + week;
      }
      $http.get(url).then(function(data) {
        defered.resolve(data);
      }),
        function onError(err) {
          console.log(err);
        };
      return promise;
    }
  });

angular.module("acStats", []).component("acStats", {
  templateUrl: "./js/components/ac-stats/ac-stats.html",
  controller: function($http, resultService) {
    var vm = this;
    resultService.getResults(0).then(function(data) {
      vm.stats = data.data.teams;
      vm.last = data.data.last;
      vm.matchDay = data.data.matchday;
    }),
      function onError(err) {
        console.log(err);
      };
  }
});

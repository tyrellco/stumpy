angular.module("acSidebar", []).component("acSidebar", {
  templateUrl: "./js/components/ac-sidebar/ac-sidebar.html",
  bindings: {
    stats: "=",
    info: "="
  },
  controller: function($http, resultService) {
    var vm = this;
    vm.loading = false;
    vm.changeWeek = function(week) {
      vm.loading = true;
      vm.week = week;
      resultService.getResults(week).then(function(data) {
        vm.stats = data.data.teams;
        vm.loading = false;
      }),
        function onError(err) {
          console.log(err);
        };
    };
  }
});
